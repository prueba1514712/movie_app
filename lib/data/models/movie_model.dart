import 'package:equatable/equatable.dart';

class MovieModel extends Equatable {
  final int id;
  final String title;
  final String overview;
  final String posterPath;
  final String releaseDate;
  final int page;
  final double voteAverage;

  const MovieModel(
      {required this.id,
      required this.title,
      required this.overview,
      required this.posterPath,
      required this.voteAverage,
      required this.releaseDate,
      required this.page});

  @override
  List<Object> get props => [id, title, overview, posterPath, releaseDate];
}
