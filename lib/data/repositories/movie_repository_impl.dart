import 'package:dio/dio.dart';
import 'package:movie_app_tmdb/data/models/movie_model.dart';
import 'package:movie_app_tmdb/domain/repositories/movie_repository.dart';
import 'package:movie_app_tmdb/utils/constants.dart';

class MovieRepositoryImpl implements MovieRepository {
  final Dio _dio;

  MovieRepositoryImpl(this._dio);

  @override
  Future<List<MovieModel>> getPopularMovies(int pagina) async {
    try {
      pagina = pagina + 1;
      final response = await _dio.get(
        'https://api.themoviedb.org/3/discover/movie',
        options: Options(headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ${Constants.apiKey}',
          'access-control-allow-header': '*',
          'Accept': '*',
        }),
        queryParameters: {
          'include_adult': false,
          'include_video': false,
          'language': 'en-US',
          'page': pagina,
          'sort_by': 'popularity.desc',
        },
      );

      final List<dynamic> results = response.data['results'];
      var page = response.data['page'];
      final List<MovieModel> movies = results.map((dynamic json) {
        final map = json as Map<String, dynamic>;
        return MovieModel(
            id: map['id'] as int,
            title: map['title'] as String,
            overview: map['overview'] as String,
            posterPath: map['poster_path'] as String,
            voteAverage: map['vote_average'] as double,
            releaseDate: map['release_date'],
            page: page);
      }).toList();
      return movies;
    } catch (error) {
      throw Exception('Failed to load movies: $error');
    }
  }
}
