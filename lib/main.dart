import 'package:flutter/widgets.dart';
import 'package:movie_app_tmdb/myapp.dart';
import 'package:movie_app_tmdb/utils/injection.dart';

void main() {
  InjectionGetIt.injectionConfiguration();

  runApp(const App());
}
