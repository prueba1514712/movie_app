
import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:movie_app_tmdb/data/repositories/movie_repository_impl.dart';
import 'package:movie_app_tmdb/domain/repositories/movie_repository.dart';
import 'package:movie_app_tmdb/presentation/bloc/movie_bloc.dart';
class InjectionGetIt{
   
static injectionConfiguration(){
     GetIt.I.registerLazySingleton<Dio>(() => Dio());
  GetIt.I.registerLazySingleton<MovieRepository>(
      () => MovieRepositoryImpl(GetIt.I<Dio>()));
  GetIt.I.registerFactory(() => MovieBloc(movieRepository:GetIt.I<MovieRepository>()));
   }
}