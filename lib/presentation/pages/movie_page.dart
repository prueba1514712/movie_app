import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';

import 'package:movie_app_tmdb/presentation/bloc/movie_bloc.dart';
import 'package:movie_app_tmdb/presentation/pages/movie_list.dart';

class MoviesPage extends StatelessWidget {
  const MoviesPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: const Center(
        child: Text('Movie App'),
      )),
      body: BlocProvider<MovieBloc>(
        create: (_) => GetIt.I<MovieBloc>()..add(MovieFetched()),
        child: const MovieList(),
      ),
    );
  }
}
