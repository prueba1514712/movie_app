import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:movie_app_tmdb/data/models/movie_model.dart';
import 'package:movie_app_tmdb/domain/repositories/movie_repository.dart';

part 'movie_event.dart';
part 'movie_state.dart';

class MovieBloc extends Bloc<MovieEvent, MovieState> {
  MovieBloc({required this.movieRepository}) : super(const MovieState()) {
    on<MovieFetched>(
      _onMovietFetched,
    );
  }
  final MovieRepository movieRepository;

  Future<void> _onMovietFetched(
    MovieFetched event,
    Emitter<MovieState> emit,
  ) async {
    try {
      if (state.status == MovieStatus.initial) {
        final movies = await movieRepository.getPopularMovies(0);
        return emit(
          state.copyWith(
            status: MovieStatus.success,
            movies: movies,
            hasReachedMax: false,
          ),
        );
      }
      final movies =
          await movieRepository.getPopularMovies(state.movies[0].page);
      movies.isEmpty
          ? emit(state.copyWith(hasReachedMax: true))
          : emit(
              state.copyWith(
                status: MovieStatus.success,
                movies: List.of(state.movies)..addAll(movies),
                hasReachedMax: false,
              ),
            );
    } catch (_) {
      emit(state.copyWith(status: MovieStatus.failure));
    }
  }
}
