part of 'movie_bloc.dart';

enum MovieStatus { initial, success, failure }

final class MovieState extends Equatable {
  const MovieState({
    this.status = MovieStatus.initial,
    this.movies = const <MovieModel>[],
  });

  final MovieStatus status;
  final List<MovieModel> movies;

  MovieState copyWith({
    MovieStatus? status,
    List<MovieModel>? movies,
    bool? hasReachedMax,
  }) {
    return MovieState(
      status: status ?? this.status,
      movies: movies ?? this.movies,
    );
  }

  @override
  List<Object> get props => [
        status,
        movies,
      ];
}
