import 'package:movie_app_tmdb/data/models/movie_model.dart';

abstract class MovieRepository {
  Future<List<MovieModel>> getPopularMovies(int pagina);
}
