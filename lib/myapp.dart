import 'package:flutter/material.dart';
import 'package:movie_app_tmdb/presentation/pages/movie_page.dart';

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(title: 'Material App', home: MoviesPage());
  }
}
